package searchEngine;

public class Messages {

    public static final String WRONG_USAGE_HEADER_MSG = "No arguments.";
    public static final String WRONG_USAGE_CONTENT_MSG = "Usage: java -jar DGSearch-1.0-SNAPSHOT.jar " +
            "\"searchWord\" \"document1\" \"document2\" \"documentn\"";
    public static final String NO_WORD_FOUND_IN_DOCUMENTS_MSG = "The word is not in any of the documents";

    static final String PREFIX_OF_THE_ID = "Document";
    static final String SPLIT_BY = " ";

}
