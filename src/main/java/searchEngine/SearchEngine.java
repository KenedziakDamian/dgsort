package searchEngine;

import javafx.util.Pair;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static searchEngine.Messages.PREFIX_OF_THE_ID;
import static searchEngine.Messages.SPLIT_BY;

public class SearchEngine {

    private String searchWord;
    private List<String> documents;

    public SearchEngine(String searchWord, String... documents) {
        this.searchWord = searchWord;
        this.documents = new LinkedList<>();
        this.documents.addAll(Arrays.asList(documents));
    }

    public List<String> run() {
        List<String> searchResult = new LinkedList<>();
        final OcurrenceChain ocurrenceChain = createOcurrenceChain();
        List<Pair<Integer, Double>> result = searchAndSortIn(ocurrenceChain);
        result.forEach(e -> {
            searchResult.add(PREFIX_OF_THE_ID + (e.getKey() + 1));
        });
        return searchResult;
    }

    private List<Pair<Integer, Double>> searchAndSortIn(OcurrenceChain ocurences) {
        Optional<HashMap<Integer, Integer>> numberOfOccurrencesForDocument =
                ocurences.getListOfindexContainsWord(searchWord);

        if (!numberOfOccurrencesForDocument.isPresent()) {
            return new LinkedList<>();
        }
        HashMap<Integer,Integer> numberOfOccurrences = numberOfOccurrencesForDocument.get();
        return tfidfSort(numberOfOccurrences);
    }

    private List<Pair<Integer, Double>> tfidfSort(HashMap<Integer,Integer> wordNumberInDocuments) {
        LinkedList<Pair<Integer, Double>> tfidfValues = new LinkedList<>();
        int numberOfWordInDocuments = wordNumberInDocuments.values().stream().mapToInt(Integer::intValue).sum();
        double idf = Math.log(documents.size() / numberOfWordInDocuments);
        int tf;
        for(Integer key:wordNumberInDocuments.keySet()){
           tf = wordNumberInDocuments.get(key)*documents.get(key).split(SPLIT_BY).length;
            tfidfValues.add(new Pair<>(key, tf * idf));
        }
        tfidfValues.sort(Comparator.comparing(Pair::getValue));
        return tfidfValues;
    }

    private OcurrenceChain createOcurrenceChain() {
        OcurrenceChain ocurences = new OcurrenceChain(new LinkedList<>(), documents.size());
        for (int docIndex = 0; docIndex < documents.size(); docIndex++) {
            for (String w : Arrays.stream(documents.get(docIndex).split(SPLIT_BY)).collect(Collectors.toList())) {
                ocurences.addNewOccurence(w, docIndex);
            }
        }
        return ocurences;
    }
}
