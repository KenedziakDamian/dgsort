package searchEngine;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;


@Getter
@Setter
class Ocurence {

    private HashMap<Integer, Integer> occurencesInDocuments;
    private String word;

    Ocurence(String word, int documentIndexNumber, int documentNumber) {
        this.word = word;
        occurencesInDocuments = new HashMap<>();
        addOcurenceIn(documentIndexNumber);
    }

    void addOcurenceIn(int index) {
        boolean containsKey = occurencesInDocuments.containsKey(index);
        if (containsKey) {
            occurencesInDocuments.replace(index, occurencesInDocuments.get(index) + 1);
        } else {
            occurencesInDocuments.put(index, 1);
        }
    }
}
