package searchEngine;

import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
class OcurrenceChain {

    private List<Ocurence> ocurences;
    private int docNumber;

    void addNewOccurence(String word, int docIndex) {
        int indexOfOcurence = searchForElementIndex(word);
        if (indexOfOcurence == -1) {
            addNewElement(word, docIndex);
        } else {
            incrementElementOfIndex(indexOfOcurence, docIndex);
        }
    }

    private int searchForElementIndex(String word) {
        return ocurences.stream()
                .map(Ocurence::getWord)
                .collect(Collectors.toList())
                .indexOf(word);
    }

    private void addNewElement(String word, int docIndex) {
        ocurences.add(searchForNewElementIndex(word), new Ocurence(word, docIndex, docNumber));
    }

    private int searchForNewElementIndex(String word) {
        int index = 0;
        for (Ocurence ocurence : ocurences) {
            if (ocurence.getWord().compareTo(word) > 0)
                return index;

            index++;
        }
        return index;
    }

    private void incrementElementOfIndex(int indexOfElement, int docIndex) {
        ocurences.get(indexOfElement).addOcurenceIn(docIndex);
    }

    Optional<HashMap<Integer, Integer>> getListOfindexContainsWord(String word) {
        int elementIndex = searchForElementIndex(word);
        if (elementIndex < 0)
            return Optional.empty();
        Ocurence ocurence = ocurences.get(elementIndex);
        return Optional.of(ocurence.getOccurencesInDocuments());
    }
}
