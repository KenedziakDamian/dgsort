import searchEngine.SearchEngine;

import java.util.Arrays;
import java.util.List;

import static searchEngine.Messages.NO_WORD_FOUND_IN_DOCUMENTS_MSG;
import static searchEngine.Messages.WRONG_USAGE_CONTENT_MSG;
import static searchEngine.Messages.WRONG_USAGE_HEADER_MSG;

public class Main {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println(WRONG_USAGE_HEADER_MSG);
            System.out.println(WRONG_USAGE_CONTENT_MSG);
            return;
        }
        List<String> result = new SearchEngine(args[0], Arrays.copyOfRange(args, 1, args.length)).run();
        if (result.isEmpty()) {
            System.out.println(NO_WORD_FOUND_IN_DOCUMENTS_MSG);
        }
        result.forEach(System.out::println);
    }
}
